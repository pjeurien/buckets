# Bucket Server


The bucket server is a simple cron solution to automate tasks on your server by simply dragging and dropping files onto it's share and letting the server process these files. It has a basic queue system and monitors whether the CPU is too busy to add additional tasks. Although this is rudimentary at best and needs to be more robust in the future.

The tasks mostly convergence on file conversion and downloading but maybe other tasks will be added in the future. At present the following buckets are currently supported:

    FFMpeg
    Youtube-dl
    Gallery-dl
    GPG

The bucket server requires a Linux based distro to function. The setup included works for Debian and most derivatives but should be easily modifiable for other Distro's.
Note this will install third-party tools and you shoulder consider the implications of this. I use these myself but I cannot guarantee their continued operation or safety.

installation steps:

Make sure git is installed on the system and configured correctly.
Clone the repository to your server's hard drive by typing the following:

    git clone https://gitlab.com/pjeurien/buckets

Navigate to the bin directory and run the install.sh file as root to install the necessary software components.

    sudo ./install.sh

Copy the buckets.cfg file from the bin directory to /etc/ and fill out the variables between the curly brackets.

    cp /bin/buckets.cfg /etc

Edit your crontab.

    crontab -e

Paste the following at the end of your crontab.

    */5 * * * * /{directory}/bin/tomp3.sh >> /{directory}/logs/tomp3.log
    */5 * * * * /{directory}/bin/tonormalize.sh >> /{directory}/logs/tonormalize.log
    */5 * * * * /{directory}/bin/to720p.sh >> /{directory}/logs/to720p.log
    */5 * * * * /{directory}/bin/to1080p.sh >> /{directory}/logs/to1080p.log
    */5 * * * * /{directory}/bin/toh264.sh >> /{directory}/logs/toh264.log
    */5 * * * * /{directory}/bin/toh265.sh >> /{directory}/logs/toh265.log
    */5 * * * * /{directory}/bin/towebm.sh >> /{directory}/logs/towebm.log
    */5 * * * * /{directory}/bin/toencrypt.sh >> /{directory}/logs/toencrypt.log
    */5 * * * * /{directory}/bin/todecrypt.sh >> /{directory}/logs/todecrypt.log
    */5 * * * * /{directory}/bin/toyoutubedl.sh >> /{directory}/logs/toyoutubedl.log
    */5 * * * * /{directory}/bin/togallerydl.sh >> /{directory}/logs/togallerydl.log

edit your samba config and add shares for the input and output directories.

    sudo nano /etc/samba/smb.conf

Add the shares at the end of the config file.

    [input]
    path = {path}
    valid users = {users}
    write list = {users}

    [output]
    path = {path}
    valid users = {users}
    write list = {users}

Depending on your configuration, everything should be up and running now. It depends on which distro you are running.
