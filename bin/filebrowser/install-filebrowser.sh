#!/bin/bash
# license: GPL v3
# author: Patriek Jeuriens.
# description: setup filebrowser with docker.
# usage: run script from cli as root.

# setup docker.io
apt install docker.io

# load configuration
source /etc/buckets.cfg

# stop container
docker stop bucketserver

# remove container
docker rm bucketserver

# setup filebrowser
docker run --name bucketserver \
    --restart unless-stopped \
    -v "$directory/storage":/srv \
    -v "$directory/bin/filebrowser/filebrowser.db":/database.db \
    -v "$directory/bin/filebrowser/filebrowser.json":/.filebrowser.json \
    -v "$directory/bin/filebrowser/config/":/config \
    --user $(id -u):$(id -g) \
    -p 80:80 \
    -d filebrowser/filebrowser

