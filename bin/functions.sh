# functions file for bucket server
fn_send_email() {
        echo "sending email to $email"
	/usr/bin/mail -s "$subject" -a "From:$service<$sender>" "$email" <<< $(echo -e "completed @ $currenttime \n$body");
}
fn_check_cpu() {
# check cpu usage threshold
	cores=$(nproc)
	load=$(awk '{print $3}'< /proc/loadavg)
	usage=$(echo | awk -v c="${cores}" -v l="${load}" '{print l*100/c}' | awk -F. '{print $1}')
	echo "checking cpu: it's currently at $usage%."
	if [[ ${usage} -ge 80 ]]; then
		echo "CPU is busy... aborting"
		exit 1
	fi
}
fn_check_service() {
	# check service state and abort if already running
	if pgrep -x "$service" >/dev/null
	then
		echo "checking service: $service is already running... aborting."
		exit 1
	else 
                echo "checking service: $service is not running... starting."
	fi
}
fn_check_directory() {
	if find $directory -maxdepth 0 -empty | read v; then exit 1; fi
}
