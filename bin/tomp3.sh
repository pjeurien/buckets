#!/bin/bash
# license: GPL v3
# author: Patriek Jeuriens.
# description: convert files to mp3.
# usage: drop files into input bucket.

# change directory
cd "$(dirname "$0")";

# load configuration
source /etc/buckets.cfg
source functions.sh
service="ffmpeg"

# sendmail message
subject="bucket-server: tomp3"

# start time
currentdate=$(date +'%m/%d/%Y')
currenttime=$(date +"%H:%M")
echo "$currenttime starting $service"

# check resources and queue if necessary
fn_check_cpu;
fn_check_service;

# process directory
echo "processing directory: $directory/storage/input/tomp3/*" 
for i in $directory/storage/input/tomp3/*;
do
	# set variables
	filename=$(basename "$i" | sed 's/\(.*\)\..*/\1/');
    file=$(basename "$i")
	output="$directory/storage/output/tomp3"

	# move to executing folder
	mv "$i" "$directory/storage/executing/$file"

	# process file
	ffmpeg -i "$directory/storage/executing/$file" -q:a 0 -map a "${output}/${file}.mp3"; 

	# create body for email.
	body+="\n $directory/storage/executing/$file"

	#delete file file
	rm "$directory/storage/executing/$file"
done

# send email if filess were processed
if [ "$i" != "$directory/storage/input/tomp3/*" ]; then
	fn_send_email;
fi
