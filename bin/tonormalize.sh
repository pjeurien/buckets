#!/bin/bash
# license: GPL v3
# author: Patriek Jeuriens.
# description: normalize audio files.
# usage: drop files into input bucket.

# change directory
cd "$(dirname "$0")";

# load configuration
source /etc/buckets.cfg
source functions.sh
service="ffmpeg"
COUNTER=0

# sendmail message
subject="bucket-server: tonormalize"

# start time
currentdate=$(date +'%m/%d/%Y')
currenttime=$(date +"%H:%M")
echo "$currenttime starting $service"

# check resources and queue if necessary
fn_check_cpu;
fn_check_service;

# process directory
echo "processing directory: $directory/storage/input/tonormalize/*" 
for i in $directory/storage/input/tonormalize/*;
do
	# set variables
	filename=$(basename "$i" | sed 's/\(.*\)\..*/\1/');
    file=$(basename "$i")
	output="$directory/storage/output/tonormalize"

	# move to executing folder
	mv "$i" "$directory/storage/executing/$file"

	# process files
	ffmpeg -i "$directory/storage/executing/$file" -filter:a loudnorm "${output}/${file}"; 	

	# delete the file
	rm "$directory/storage/executing/$file"
	let counter++
done

# send email if filess were processed
if [ "$i" != "$directory/storage/input/tonormalize/*" ]; then
	fn_send_email;
fi
