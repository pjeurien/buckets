#!/bin/bash
# license: GPL v3
# author: Patriek Jeuriens.
# description: send urls to gallery-dl.
# usage: place text file with urls to download in input bucket.

# change directory
cd "$(dirname "$0")";

# load configuration
source /etc/buckets.cfg
source functions.sh
service="gallery-dl"

# sendmail message
subject="bucket-server: togallerydl"

# start time
currentdate=$(date +'%m/%d/%Y')
currenttime=$(date +"%H:%M")
echo "$currenttime starting $service"

# check resources and queue if necessary
fn_check_cpu;
fn_check_service;

# process directory
echo "processing directory: $directory/storage/input/togallerydl/*" 
for i in $directory/storage/input/togallerydl/*;
do
	# set variables
	filename=$(basename "$i" | sed 's/\(.*\)\..*/\1/');
	file=$(basename "$i")
	output="$directory/storage/output/togallerydl/"

	# read urls from text file.
	while IFS= read -r url || [ -n "$url" ]; do
		echo ${url}
	        /usr/local/bin/gallery-dl -d "$output" ${url//$'\r'/}
		printf '%s\n' "$url"
	done < "$i"

	# move to archive folder
	mv "$i" "$directory/storage/archive/"
done

# send email if filess were processed
if [ "$i" != "$directory/storage/input/togallerydl/*" ]; then
	fn_send_email;
fi

