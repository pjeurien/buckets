#!/bin/bash
# license: GPL v3
# author: Patriek Jeuriens.
# description: decrypt encrypted files with password.
# usage: drop files into input bucket.

# change directory
cd "$(dirname "$0")";

# load configuration
source /etc/buckets.cfg
source functions.sh
service="gpg"

# start time
currentdate=$(date +'%m/%d/%Y')
currenttime=$(date +"%H:%M")
echo "$currenttime starting $service"

# check resources and queue if necessary
fn_check_cpu;
fn_check_service;

# process directory
echo "processing directory: $directory/storage/input/todecrypt/*" 
for i in $directory/storage/input/todecrypt/*;
do
	# set variables
	filename=$(basename "$i" | sed 's/\(.*\)\..*/\1/');
	file=$(basename "$i")
	output="$directory/storage/output"
	executing="$directory/storage/executing/$file"

	# move to executing folder.
	mv "$i" "$executing"

	# process file
	gpg --passphrase "$password" --batch -o "${output}/todecrypt/${filename}" --decrypt "$executing"

	# create body for email.
	body+="\n $directory/storage/executing/$file"

	# delete the file
	rm "$directory/storage/executing/$file" 

done

# send email if filess were processed
if [ "$i" != "$directory/storage/input/todecrypt/*" ]; then
	fn_send_email;
fi
