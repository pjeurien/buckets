#!/bin/bash
# license: GPL v3
# author: Patriek Jeuriens.
# description: convert to 1080p h264.
# usage: drop files into input bucket.

# change directory
cd "$(dirname "$0")";

# load configuration
source /etc/buckets.cfg
source functions.sh
service="ffmpeg"

# sendmail message
subject="bucket-server: to1080p"

# check resources and queue if necessary
fn_check_cpu;
fn_check_service;

# process directory
echo "processing directory: $directory/storage/input/to1080p/*" 
for i in $directory/storage/input/to1080p/*;
do
	# set variables
	filename=$(basename "$i" | sed 's/\(.*\)\..*/\1/');
    file=$(basename "$i")
	output="$directory/storage/output/to1080p"

	# move to executing folder
	mv "$i" "$directory/storage/executing/$file"

	# process the file
	ffmpeg -i "$directory/storage/executing/$file" -vf scale=-1:1080 -c:v libx264 -crf 18 -preset slow -c:a copy "${output}/${filename}.mp4"; 

	# create body for email.
	body+="\n $directory/storage/executing/$file"

	# delete the file
	rm "$directory/storage/executing/$file"
done
# send email if filess were processed
if [ "$i" != "$directory/storage/executing/to1080p/*" ]; then
	fn_send_email;
fi
