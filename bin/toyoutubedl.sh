#!/bin/bash
# license: GPL v3
# author: Patriek Jeuriens.
# description: send urls to youtubedl
# usage: place text file with urls to download in input bucket.

# change directory
cd "$(dirname "$0")";

# load configuration
source /etc/buckets.cfg
source functions.sh
service="youtube-dl"

# sendmail message
subject="bucket-server: toyoutubedl"

# start time
currentdate=$(date +'%m/%d/%Y')
currenttime=$(date +"%H:%M")
echo "$currenttime starting $service"

# check resources and queue if necessary
fn_check_cpu;
fn_check_service;

# process directory
echo "processing directory: $directory/storage/input/toyoutubedl/*" 
for i in $directory/storage/input/toyoutubedl/*;
do
	# set variables
	filename=$(basename "$i" | sed 's/\(.*\)\..*/\1/');
    file=$(basename "$i")
	output="$directory/storage/output/toyoutubedl/%(title)s.%(ext)s"	

	# loop through urls in the textfile
	# one url per line
	while IFS= read -r url || [ -n "$url" ]; do
		sendemail="true" # set email flag
		printf '%s\n' "$url" # print url
		/usr/local/bin/youtube-dl --no-progress -o "$output" $url
	done < "$i"
	
	# move the text file to archive
	mv "$i" "$directory/storage/archive/"

	# send email if urls were processed
	if [ "$i" != "$directory/storage/input/toyoutubedl/*" ]; then 
		echo "sending email to $email"
		mail -s "$subject" -a "From:$service<$sender>" "$email" <<< "$i completed @ $currenttime";
	fi
done
