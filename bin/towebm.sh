#!/bin/bash
# license: GPL v3
# author: Patriek Jeuriens.
# description: convert files to webm.
# usage: drop files into input bucket.

# change directory
cd "$(dirname "$0")";

# load configuration and functions
source /etc/buckets.cfg
source functions.sh
service="ffmpeg"

# sendmail message
subject="bucket-server: towebm"

# start time
currentdate=$(date +'%m/%d/%Y')
currenttime=$(date +"%H:%M")
echo "$currenttime starting $service"

# check resources and queue if necessary
fn_check_cpu;
fn_check_service;

# process directory
echo "processing directory: $directory/storage/input/towebm/*" 
for i in $directory/storage/input/towebm/*;
do
	# set variables 
	filename=$(basename "$i" | sed 's/\(.*\)\..*/\1/');
	file=$(basename "$i")
	output="$directory/storage/output/towebm"

	# move to executing folder
	mv "$i" "$directory/storage/executing/$file"

	# process the file two passes for better quality.
	ffmpeg -i "$directory/storage/executing/$file" -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 1 -an -f null /dev/null && \
	ffmpeg -i "$directory/storage/executing/$file" -c:v libvpx-vp9 -b:v 0 -crf 30 -pass 2 -c:a libopus "${output}/${filename}.webm";
	#ffmpeg -i "$directory/storage/executing/$file" -crf 5 -c:a libopus -b:a 64k "${output}/${filename}.webm";

	# create body for email.
	body+="\n $directory/storage/executing/$file"

	# delete the file
	rm "$directory/storage/executing/$file" 
done

# send email if files were processed
if [ "$i" != "$directory/storage/input/towebm/*" ]; then
	fn_send_email;
fi
