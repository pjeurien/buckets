#!/bin/bash
# license: GPL v3
# author: Patriek Jeuriens.
# description: convert files to h265.
# usage: drop files into input bucket.

# change directory
cd "$(dirname "$0")";

# load configuration
source /etc/buckets.cfg
source functions.sh
service="ffmpeg"

# sendmail message
subject="bucket-server: toh265"

# start time
currentdate=$(date +'%m/%d/%Y')
currenttime=$(date +"%H:%M")
echo "$currenttime starting $service"

# check resources and queue if necessary
fn_check_cpu;
fn_check_service;

# process directory
echo "processing directory: $directory/storage/input/toh265/*" 
for i in $directory/storage/input/toh265/*;
do
	# set variables
	filename=$(basename "$i" | sed 's/\(.*\)\..*/\1/');
    	file=$(basename "$i")
	output="$directory/storage/output/toh265"
	executing="$directory/storage/executing/$file"

	# move to executing folder
	mv "$i" "$executing"

	# process file
	ffmpeg -i "$executing" -an -vcodec libx265 -crf 23 "${output}/${file}.mp4"; 

	# create body for email
	body+="\n $directory/storage/executing/$file"

	# delete file file
	rm "$directory/storage/executing/$file"
done

# send email if filess were processed
if [ "$i" != "$directory/storage/input/toh265/*" ]; then
	fn_send_email;
fi
