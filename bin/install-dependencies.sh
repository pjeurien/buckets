#!/bin/bash
# license: GPL v3
# author: Patriek Jeuriens.
# description: setup bucket server
# usage: run script from cli as root.

# install from repo
apt install cifs-utils
apt install ffmpeg

# download youtube-dl
apt install python-is-python3
curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
chmod a+rx /usr/local/bin/youtube-dl

# setup gallery-dl
apt install python3-pip
apt install python3-testresources
python3 -m pip install --upgrade pip setuptools wheel
python3 -m pip install -U -I --no-deps --no-cache-dir https://github.com/mikf/gallery-dl/archive/master.tar.gz
